(function( $ ) {
    'use strict';

    // Will hold the templates
    var $res_halls_grid_templ = false;
    var $res_halls_select_templ = false;

    // We only want to load the residence halls select once so is true once loaded
    var $res_halls_select_loaded = false;

    // Will hold the search field
    var $search_field = false;

    // When the document is ready...
    $(document).ready(function() {

        // Get the templates
        var $res_halls_grid_templ_content = $( '#sa-res-halls-grid-template' ).html();
        var $res_halls_select_templ_content = $( '#sa-res-halls-select-template' ).html();

        // Parse the templates
        $res_halls_grid_templ = Handlebars.compile( $res_halls_grid_templ_content );
        $res_halls_select_templ = Handlebars.compile( $res_halls_select_templ_content );

        // Get the residence halls
        render_sa_res_halls();

        // The search field
        $search_field = $( '#sa-res-halls-search-field' );

        // Setup the timer
        var $search_typing_timer;
        var $search_typing_interval = 700; // time in ms

        // On keyup, start the countdown
        $search_field.on( 'keyup', function () {
            clearTimeout( $search_typing_timer );
            $search_typing_timer = setTimeout( sa_department_search_done_typing, $search_typing_interval );
            check_sa_department_search_status();
        });

        // On keydown, clear the countdown
        $search_field.on( 'keydown', function () {
            clearTimeout( $search_typing_timer );
            check_sa_department_search_status();
        });

        // Handle search
        $search_field.on( 'change paste propertychange', function($event) {

            // Update the residence halls
            render_sa_res_halls( $(this).val() );

        });

        // Halt the form
        $( '#sa-res-halls-search-form').on( 'submit', function() {

            // Update the residence halls
            render_sa_res_halls( $(this).val() );

            return false;
        });

        // Close active search
        $( '#sa-res-halls-search-form .close-search-button' ).on( 'click', function() {

            // Clear the search field
            $search_field.val('');

            // Update the residence halls and status
            check_sa_department_search_status();
            render_sa_res_halls();

        });

    });

    ///// FUNCTIONS /////

    // Checks to see if search is active
    function check_sa_department_search_status() {

        if ( $search_field.val() != '' ) {
            $('#sa-res-halls-search-form').addClass( 'active' );
        } else {
            $('#sa-res-halls-search-form').removeClass( 'active' );
        }

    }

    // User looks to be finished typing
    function sa_department_search_done_typing () {

        // Don't update if less than 2 characters
        if ( $search_field.val().length > 0 && $search_field.val().length < 2 ) {
            return false;
        }

        // Update the residence halls
        render_sa_res_halls( $search_field.val() );

    }

    // Get/update the residence halls
    function render_sa_res_halls( $search ) {

        // Build query string
        // &filter[orderby]=title&filter[order]=asc
        var $query_string = 'filter[posts_per_page]=-1';

        // Add search
        if ( $search !== undefined && $search != '' ) {
            $query_string += '&filter[s]=' + $search;
        }

        // Get residence halls
        $.ajax( {
            url: '/wp-json/wp/v2/res_halls?' + $query_string,
            success: function ( $res_halls ) {

                // Render the templates
                var $rendered_grid = $res_halls_grid_templ( $res_halls );

                // Add to the grid
                $( '#sa-res-halls-grid' ).html( $rendered_grid );

                // Unveil the images
                $( '#sa-res-halls-grid img' ).unveil( 0, function() {
                    $(this).load(function() {
                        $(this).closest('.sa-item').css({'opacity':'1'});
                    });
                });
                $( '#sa-res-halls-grid img:in-viewport' ).trigger( 'unveil' );

                // Load those without an image
                $( "#sa-res-halls-grid img[data-src='']").closest('.sa-item').css({'opacity':'1'});

                // Only load the select once
                if ( ! $res_halls_select_loaded ) {

                    // Don't load anymore
                    $res_halls_select_loaded = true;

                    // Render the templates
                    var $rendered_select = $res_halls_select_templ( $res_halls );

                    // Add to the grid and handle select
                    $( '#sa-res-halls-select' ).append( $rendered_select).on( 'change', function() {

                        // Redirect to URL
                        if( $(this).val() != '' ) {
                            window.location.href = $(this).val();
                        }

                        return false;

                    });

                }

            },
            cache: true
        } );

    }

})( jQuery );