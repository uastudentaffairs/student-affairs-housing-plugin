<?php

/**
 * Plugin Name:       Student Affairs - Housing and Residential Communities
 * Plugin URI:        https://sa.ua.edu
 * Description:       This WordPress plugin is intended solely for The University of Alabama Division of Student Affairs.
 * Version:           1.0
 * Author:            UA Division of Student Affairs - Rachel Carden
 * Author URI:        https://sa.ua.edu
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Add files
require_once plugin_dir_path( __FILE__ ) . 'includes/filters.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/query.php';

// Add admin functionality in admin
if( is_admin() ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';
}

// Internationalization
// @TODO Add language files
/*add_action( 'init', 'ua_sa_housing_textdomain' );
function ua_sa_housing_textdomain() {
	load_plugin_textdomain( 'ua-sa-housing', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}*/

// Runs on install
register_activation_hook( __FILE__, 'ua_sa_housing_install' );
function ua_sa_housing_install() {

	// Register the custom post type
	ua_sa_housing_register_cpt();

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Runs when the plugin is upgraded
// @TODO test to see if this only runs when running a bulk upgrade
add_action( 'upgrader_process_complete', 'ua_sa_housing_upgrader_process_complete', 1, 2 );
function ua_sa_housing_upgrader_process_complete( $upgrader, $upgrade_info ) {

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

//! Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {

	// Get the plugin CSS directory
	$sa_housing_dir = plugin_dir_url( __FILE__ );
	$sa_template_dir = trailingslashit( get_template_directory_uri() );

	// Enqueue the residence halls script
	if ( is_page( 'residence-halls' ) ) {

		// Register unveil and viewport
		wp_register_script( 'viewport', $sa_template_dir . 'js/viewport.min.js', array( 'jquery' ) );
		wp_register_script( 'unveil', $sa_template_dir . 'js/unveil.min.js', array( 'jquery', 'viewport' ) );

		// Enqueue the residence halls script
		wp_enqueue_script( 'sa-housing-res-halls', $sa_housing_dir . 'js/sa-housing-res-halls.min.js', array( 'jquery', 'handlebars', 'unveil' ) );
	}

}, 30 );

// Register housing custom post types
add_action( 'init', 'ua_sa_housing_register_cpt' );
function ua_sa_housing_register_cpt() {

	// Register residence halls CPT
	register_post_type( 'res_halls', array(
		'labels' => array(
			'name'               => _x( 'Residence Halls', 'post type general name', 'ua-sa-housing' ),
			'singular_name'      => _x( 'Residence Hall', 'post type singular name', 'ua-sa-housing' ),
			'menu_name'          => _x( 'Residence Halls', 'admin menu', 'ua-sa-housing' ),
			'name_admin_bar'     => _x( 'Residence Halls', 'add new on admin bar', 'ua-sa-housing' ),
			'add_new'            => _x( 'Add New', 'res_halls', 'ua-sa-housing' ),
			'add_new_item'       => __( 'Add New Residence Hall', 'ua-sa-housing' ),
			'new_item'           => __( 'New Residence Hall', 'ua-sa-housing' ),
			'edit_item'          => __( 'Edit Residence Hall', 'ua-sa-housing' ),
			'view_item'          => __( 'View Residence Hall', 'ua-sa-housing' ),
			'all_items'          => __( 'All Residence Halls', 'ua-sa-housing' ),
			'search_items'       => __( 'Search Residence Halls', 'ua-sa-housing' ),
			'parent_item_colon'  => __( 'Parent Residence Hall:', 'ua-sa-housing' ),
			'not_found'          => __( 'No residence halls found.', 'ua-sa-housing' ),
			'not_found_in_trash' => __( 'No residence halls found in Trash.', 'ua-sa-housing' )
		),
		'public'                => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-admin-home',
		'capabilities'          => array(
			'edit_post'         => 'edit_res_hall',
			'read_post'         => 'read_res_hall',
			'delete_post'       => 'delete_res_hall',
			'edit_posts'        => 'edit_res_halls',
			'edit_others_posts' => 'edit_others_res_halls',
			'publish_posts'     => 'publish_res_halls',
			'read_private_posts'=> 'read_private_res_halls',
			'read'              => 'read',
			'delete_posts'      => 'delete_res_halls',
			'delete_private_posts' => 'delete_private_res_halls',
			'delete_published_posts' => 'delete_published_res_halls',
			'delete_others_posts' => 'delete_others_res_halls',
			'edit_private_posts' => 'edit_private_res_halls',
			'edit_published_posts' => 'edit_published_res_halls',
			'create_posts'      => 'edit_res_halls'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'residence-halls',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
		'show_in_rest'          => true,
	) );

}

