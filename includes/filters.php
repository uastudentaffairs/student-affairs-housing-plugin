<?php

// Manage some filters
add_action( 'pre_get_posts', function( $query ) {

	// Don't trim the excerpt on the residence halls page
	// Can't put this in the residence halls template file
	// because then it wouldn't trigger for the API request
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'res_halls' == $post_type
	     || ( is_array( $post_type ) && in_array( 'res_halls', $post_type ) && count( $post_type ) == 1 ) ) {

		remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );

	}

});

// Add grid image to post
add_action( 'rest_api_init', function() {

	// Get grid photo
	register_rest_field( 'res_halls', 'grid_photo', array(
		'get_callback'    => 'ua_sa_housing_rest_get_field_value',
		'update_callback' => null,
		'schema'          => null,
	));

});

function ua_sa_housing_rest_get_field_value( $object, $field_name, $request ) {

	switch( $field_name ) {

		case 'grid_photo':
			if ( ( $grid_photo_id = get_post_meta( $object[ 'id' ], 'archive_grid_photo', true ) )
			     && ( $grid_photo_src = wp_get_attachment_image_src( $grid_photo_id, 'sa-archive-grid' ) ) ) {
				return $grid_photo_src[0];
			}
			break;

	}
	return '';
}

// Use housing res hall archive template
add_filter( 'page_template', function( $template ) {
	if ( is_page( 'residence-halls' ) ) {
		return plugin_dir_path( __FILE__ ) . '/archive-res_halls.php';
	}
	return $template;
});

// Use housing single res hall template
add_filter( 'single_template', function( $template ) {
	if ( is_singular( 'res_halls' ) ) {
		return plugin_dir_path( __FILE__ ) . '/single-res_halls.php';
	}
	return $template;
});

// Get the residence halls submenu
add_filter( 'sa_framework_menu_section_post_id', function( $page_section_id ) {
	if ( is_singular( 'res_halls' ) ) {
		return 61; // ID for main residence halls page
	}
	return $page_section_id;
});