<?php

// Remove page breadcrumbs
remove_action( 'sa_framework_before_content_area', 'sa_framework_print_page_breadcrumbs' );

// Change the layout to full width
add_filter( 'sa_framework_page_layout', function ( $defined_page_layout ) {
	return 'full-width-column';
});

// Add residence halls after content
add_action( 'sa_framework_after_content', function() {

	// Get the search query
	$search_query = null;

	// Get framework dir
	$sa_framework_dir = trailingslashit( get_template_directory_uri() );

	?><form role="search" method="get" id="sa-res-halls-search-form" class="search-form search-bar<?php echo ! empty( $search_query ) ? ' active' : null; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="sa-res-halls-select">
			<span class="screen-reader-text"><?php echo _x( 'Search our residence halls:', 'label' ); ?></span>
			<select id="sa-res-halls-select" class="select-field" title="Select one of our residence halls">
				<option value="">Select a residence hall</option>
			</select>
		</label>
		<div class="search-field-wrapper">
			<label for="sa-res-halls-search-field"><span class="screen-reader-text"><?php echo _x( 'Search our residence halls:', 'label' ); ?></span></label>
			<input type="search" id="sa-res-halls-search-field" class="search-field" placeholder="Search our residence halls" value="<?php echo $search_query; ?>" name="s" title="Search our residence halls" />
			<div class="close-search-button"></div>
		</div>
		<input type="submit" class="button button-primary search-submit" autocomplete="off" value="Search" />
	</form>

	<script id="sa-res-halls-grid-template" type="text/x-handlebars-template">
		{{#.}}
			<li>
				<div class="sa-item {{#grid_photo}}has-header-img{{/grid_photo}}">
					<div class="item-header">
						{{#link}}<a href="{{.}}">{{/link}}
							{{#grid_photo}}<img src="<?php echo $sa_framework_dir; ?>images/transp/transp-600x200.png" data-src="{{.}}" />{{/grid_photo}}
							{{#title}}<h2 class="item-title">{{{rendered}}}</h2>{{/title}}
						{{#link}}</a>{{/link}}
					</div>
					{{#excerpt}}<div class="item-content">{{{rendered}}}</div>{{/excerpt}}
				</div>
			</li>
		{{/.}}
	</script><?php

	?><script id="sa-res-halls-select-template" type="text/x-handlebars-template">
		{{#.}}
			{{#title}}<option value="{{link}}">{{{rendered}}}</option>{{/title}}
		{{/.}}
	</script>

	<ul id="sa-res-halls-grid" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3 sa-items sa-items-grid unveil"></ul><?php

});

get_header();

get_footer();