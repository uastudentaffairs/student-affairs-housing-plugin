<?php

// Change the layout to full width
add_filter( 'sa_framework_page_layout', function ( $defined_page_layout ) {
	return 'full-width-column';
});

// Add info after content
add_action( 'sa_framework_after_content', function() {

	// Add residence hall details
	if( have_rows('residence_details_sections') ):
		while( have_rows('residence_details_sections') ): the_row();

			$title = get_sub_field('section_title');
			$information = get_sub_field( 'section_information' );
			$notes = get_sub_field( 'residence_details_notes' );
			if ( $title && $information ) {

				?><h3><?php echo $title; ?></h3><?php
				echo wpautop($information);

				if ( $notes ) {
					?><div class="notes"><?php echo $notes; ?></div><?php
				}

			}

		endwhile;
	endif;

	// Add room details
	if( have_rows('room_details_sections') ):

		?><h2>Room Details</h2><?php
		while( have_rows('room_details_sections') ): the_row();

			$title = get_sub_field('section_title');
			$information = get_sub_field( 'section_information' );
			$notes = get_sub_field( 'room_details_notes' );
			if ( $title && $information ) {

				?><h3><?php echo $title; ?></h3><?php
				echo wpautop($information);

				if ( $notes ) {
					?><div class="notes"><?php echo $notes; ?></div><?php
				}

			}

		endwhile;
	endif;

	// Add map
	?><iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place:Blount+Undergraduate+Initiative+Living-Learning+Center,+Tuscaloosa,+AL+35401&key=AIzaSyBlYFLdGG_u3-6zrn_efT6ODUOh-W9OxEE" allowfullscreen></iframe><?php

});

get_header();

get_footer();


/*
https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d834.4753973996684!2d-87.54868821183803!3d33.21667609879674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x888602a36a57bf39%3A0xb6ab2e1f9ea900c!2sBlount+Undergraduate+Initiative+Living-Learning+Center%2C+Tuscaloosa%2C+AL+35401!5e0!3m2!1sen!2sus!4v1447170879232
https://www.google.com/maps/place/Blount+Undergraduate+Initiative+Living-Learning+Center,+Tuscaloosa,+AL+35401/@33.2166761,-87.5486882,19z/data=!4m7!1m4!3m3!1s0x888602a341d9d7bb:0x772997ded802e98c!2s901+2nd+St,+Tuscaloosa,+AL+35401!3b1!3m1!1s0x888602a36a57bf39:0xb6ab2e1f9ea900c


AIzaSyBlYFLdGG_u3-6zrn_efT6ODUOh-W9OxEE*/