<?php

add_action( 'pre_get_posts', function( &$query ) {

	// Always order residence halls by name ASC
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'res_halls' == $post_type
	     || ( is_array( $post_type ) && in_array( 'res_halls', $post_type ) && count( $post_type ) == 1 ) ) {

		$query->set( 'orderby' , 'title' );
		$query->set( 'order' , 'ASC' );

	}

}, 100 );

// Tweak query orderby to remove "the" from beginning of titles
add_filter( 'posts_orderby', function( $orderby, $query ) {
	global $wpdb;

	// Always order residence halls by name ASC
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'res_halls' == $post_type
	     || ( is_array( $post_type ) && in_array( 'res_halls', $post_type ) && count( $post_type ) == 1 ) ) {

		$orderby = "REPLACE( lower( {$wpdb->posts}.post_title ), 'the ', '' ) ASC";

	}

	return $orderby;
}, 100, 2 );